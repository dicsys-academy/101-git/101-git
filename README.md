# Guía de estudio: Introducción a Git

## Introducción

*Git* es un sistema de control de versiones muy popular y ampliamente utilizado en la industria del software. Con *Git*, los desarrolladores pueden realizar un seguimiento de los cambios en el código fuente, colaborar con otros miembros del equipo y revertir los cambios en caso de ser necesario. 

Este curso está diseñado para aquellos que son nuevos en *Git* y desean aprender los conceptos básicos en un corto período de tiempo.

## Requisitos Previos

Todo el tutorial está pensado usando entornos GNU/Linux. 

Pero si estas en Windows no hay problema, para que todo funcione es necesario tener instalado:

Un IDE de desarrollo como VS Code y una Terminal para Windows ‘con onda’ 😎. 

https://code.visualstudio.com/

https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=es-ar&gl=ar

Y siempre es bueno tener a mano el sitio oficial https://git-scm.com/

## Configurando nuestro espacio con git config

El comando `git config` se utiliza para configurar la información de *Git*, como el nombre de usuario, la dirección de correo electrónico y las opciones de *Git*. Algunos de los usos más comunes de este comando son:

1. Verificar la configuración actual con el comando

```sh
git config --list
```

2. Configurar el nombre de usuario con el comando

```sh
git config --global user.name "<nombre-usuario>"
```

3. Configurar la dirección de correo electrónico con el comando 

```sh
git config --global user.email "<direccion-correo>"
```

4. Configurar el editor de Git con el comando 

```sh
git config --global core.editor "<nombre-editor>"
```

5. Configurar la rama por defecto 

```sh
git config --global init.defaultBranch main
```

## Crear un repositorio con git init

Para comenzar a trabajar con *Git*, el primer paso es crear un repositorio. En este caso, utilizaremos el comando `git init` para inicializar un nuevo repositorio. Este comando creará un directorio `.git` que contiene todos los archivos necesarios para que *Git* funcione correctamente. 

Ejemplo práctico:

1. Abrí la terminal o línea de comandos y navegá hasta el directorio donde deseas crear el repositorio. 

```sh
cd /home/jrg/101-git
```

2. Creamos el repositorio

```sh
git init
```

2.2 Listamos los archivos ocultos para ver la carpeta ./git

```sh
ls -a
```

Con esto hemos creado nuestro repositorio y está vacio. Podemos crear nuestros propios archivos y almacenarlos o, como suele suceder, podemos clonar repositorios que ya tienen sus propios archivos.

## Repositorios remotos

Un repositorio remoto se trata de otra instancia diferente de *Git* que tiene una copia exacta de nuestros repositorio almacenado en ./git.

![Image](https://nvie.com/img/centr-decentr@2x.png)

Para clonar un repositorio existente, se pueden seguir los siguientes pasos:

1. Obtener la URL del repositorio existente. Por ejemplo:

```sh
https://gitlab.com/dicsys-academy/101-git/101-git.git
```

2. Utilizar el comando `git clone <url-repositorio>` para clonar el repositorio en una nueva carpeta.

```sh
git clone https://gitlab.com/dicsys-academy/101-git/101-git.git
```

3. Utilizar el comando `git remote -v` para verificar que se haya agregado el repositorio remoto.

```sh
git remote -v
```

## Ramas

Las ramas son una característica importante de *Git*, que permiten a los desarrolladores trabajar en diferentes versiones de un proyecto al mismo tiempo. Con *Git*, podemos crear una rama, hacer cambios en ella y fusionarla con la rama principal.

### Actualizando ramas

Una vez que ya tenemos nuestro remoto configurado es necesario obtener el contenido del mismo. Para ello hay dos formas de hacerlo: `git fetch` y `git pull`. Veamos sus principales diferencias:

### git fetch:

Descarga los últimos cambios de un repositorio remoto sin fusionarlos con tu rama actual.
Actualiza las referencias locales de ramas remotas, permitiéndote ver los cambios en el repositorio remoto sin afectar tu trabajo actual. 
No modifica tu rama actual ni crea nuevos commits.
Puedes revisar los cambios descargados utilizando git diff o fusionarlos manualmente con git merge o git rebase.

Es útil para obtener una vista previa de los cambios antes de fusionarlos y para sincronizar tu repositorio con el remoto.

### git pull:

Descarga los últimos cambios de un repositorio remoto y los fusiona automáticamente con tu rama actual.
Combina los pasos de git fetch y git merge en un solo comando conveniente.
Actualiza tu rama actual con los nuevos commits descargados del repositorio remoto.
Puede generar un nuevo commit de fusión si hay conflictos entre tus cambios locales y los cambios remotos.

Muy útil cuando deseas actualizar rápidamente tu rama y tener tu repositorio local al día con el remoto.

### Seleccionando ramas remotas

1. Verificar las ramas remotas disponibles:

```sh
git branch -r
```

Mostrará una lista de ramas remotas como `origin/rama1`, `origin/rama2`, etc.

2. Crear una rama local basada en una rama remota:

```sh
git checkout -b rama-local origin/rama-remota
```

Crea una nueva rama local llamada `rama-local` basada en la rama remota `origin/rama-remota` y cambia a esa rama.

3. Cambiar a una rama remota existente:

```sh
git checkout rama-remota
```

Cambia a la rama remota `rama-remota` y crea automáticamente una rama local basada en ella.

4. Actualizar una rama remota localmente:

```sh
git pull origin rama-remota
```

Descarga los últimos cambios de la rama remota `origin/rama-remota` y los fusiona automáticamente con tu rama local.

Recuerda reemplazar `rama-local` con el nombre que desees para la nueva rama local, y `rama-remota` con el nombre de la rama remota que deseas seleccionar.

Seleccionar ramas remotas te permite trabajar en ramas locales basadas en las ramas remotas, lo que te permite realizar cambios y colaborar con el equipo de desarrollo.

## Fusionando cambios

El comando `git merge` se utiliza para fusionar dos ramas. Con este comando, podemos combinar los cambios de una rama con la rama principal.

1. Crea una rama llamada "feature" 

```sh
git branch feature
```

2. Cambia a la rama "feature" 

```sh
git checkout feature
```

3. Realiza algunos cambios en el archivo `README.md` en la rama "feature".

4. Fusiona la rama "feature" con la rama principal 

```sh
git merge feature
```

Si hubiese ocurrido un conflicto, en la próxima sección lo resolvemos.

## Resolver conflictos de fusión

En ocasiones, puede haber conflictos de fusión cuando se intenta combinar dos ramas con cambios diferentes en el mismo archivo. Para resolver estos conflictos, se pueden seguir los siguientes pasos:

1. Utilizar el comando `git status` para identificar los archivos con conflictos de fusión.

```sh
git status
```

2. Abrir cada archivo en conflicto y editar las secciones conflictivas para resolver el conflicto.

3. Guardar los cambios y agregarlos al área de preparación con el comando:

```sh
git add -A
```

4. Utilizar el comando `git commit` para completar la fusión y especificar un mensaje

```sh
git commit -m “MENSAJE”
```

5. Si se utilizó `git pull` para obtener los cambios de otro colaborador, se debe utilizar `git push` para enviar los cambios completados al repositorio remoto.

```sh
git push
```

## Etiquetas

También es posible usar etiquetas (tags) para trabajar con ramas e identificarlas más fácilmente, créanme, es mucho más útil de lo que imaginan.

El comando `git tag` etiqueta un commit específico con un nombre. Las etiquetas se pueden utilizar para marcar hitos importantes en la historia de un proyecto.

1. Verificar etiquetas remotas

```sh
git tag
```

2. Buscar el ID del commit para etiquetar.

```sh
git log
```

3. Etiquetamos

```sh
git tag <nombre-etiqueta> <commit-id>
```

4. Subimos al remoto las etiquetas creadas

```sh
git push --tags
```

## Comandos adicionales de Git

### git status

El comando `git status` se utiliza para verificar el estado actual del repositorio, incluyendo los archivos modificados, los archivos en el área de preparación y los archivos sin seguimiento. La salida de este comando proporciona información útil para decidir qué cambios comprometer.

### git stash

El comando `git stash` se utiliza para guardar temporalmente los cambios en el directorio de trabajo que no se desea comprometer en ese momento, pero que se desea mantener para más tarde. Los pasos para utilizar este comando son los siguientes:

1. Guardar temporalmente los cambios en el directorio de trabajo

```sh
git stash save
```

2. Listar todas las entradas guardadas en el stash

```sh
git stash list
```

3. Aplicar los cambios guardados

```sh
git stash apply <identificador-stash>
```

4. Eliminar una entrada en el stash

```sh
git stash drop <identificador-stash>
```

5. Aplicar los cambios guardados y eliminarlos 
del stash

```sh
git stash pop <identificador-stash>
```

### git log

El comando `git log` se utiliza para mostrar un historial detallado de los cambios en el repositorio. La salida de este comando incluye información sobre cada confirmación, como el autor, la fecha, el mensaje de confirmación y el ID de confirmación.

### git cherrypick

El comando `git cherrypick` se utiliza para aplicar un cambio específico de una rama a otra. Con este comando, podemos tomar un cambio de una rama y aplicarlo en otra.

Ejemplo práctico:

1. Obtener el ID del commit que deseas aplicar

```sh
git log 
```

2. Aplicar el cambio indicando el ID del commit seleccionado

```sh
git cherry-pick <commit-id>
```

3. Verifica que el cambio se haya aplicado correctamente.

### git remote

El comando `git remote` se utiliza para administrar los repositorios remotos. Algunas de las funciones más útiles de este comando son:

1. Verificar la lista de repositorios remotos con el comando 

```sh
git remote -v
```

2. Agregar un repositorio remoto con el comando

```sh
git remote add <nombre-remoto> <url-remoto>
```

3. Renombrar un repositorio remoto con el comando 

```sh
git remote rename <nombre-remoto-actual> <nombre-remoto-nuevo>
```

4. Eliminar un repositorio remoto con el comando 

```sh
git remote remove <nombre-remoto>
```