# GitFlow

Gitflow es un modelo alternativo de creación de ramas en Git en el que se utilizan ramas de función y varias ramas principales. Este flujo de trabajo no añade ningún concepto o comando nuevo más allá de los que se necesitan para el flujo de trabajo con ramas en Git. Lo que se hace con Gitflow es asignar funciones muy específicas a las distintas ramas y definir cómo y cuándo deben estas interactuar. 
Además de las ramas de función, utiliza ramas individuales para preparar, mantener y registrar publicaciones. 

## Ramas principales

Gitflow utiliza dos ramas principales para gestionar el flujo de trabajo: `master` y `develop`. La rama `master` es la rama principal de producción y se utiliza para lanzar versiones estables del proyecto. La rama `develop` es la rama principal de desarrollo y se utiliza para integrar las características y correcciones de errores en el proyecto.

```sh
git branch develop
git push -u origin develop
```

## Ramas de soporte

Además de las ramas principales, Gitflow utiliza ramas de soporte para las tareas de mantenimiento y las versiones de lanzamiento. Las ramas de soporte se ramifican de `main` por lo general. Algunos ejemplos de ramas de soporte son `hotfixes` y `release`.

1. Nos aseguramos de estar en la rama main

```sh
git checkout main
```

2. Creamos una nueva rama de hotfix por ejemplo

```sh
git checkout -b hotfixs/mi_hotfix
```

3. Subimos los cambios

```sh
git push -u origin hotfixs/mi_hotfix
```

## Ramas de características

Las ramas de características son ramas temporales que se utilizan para implementar nuevas características o funcionalidades. Estas ramas se ramifican de la rama `develop` y se fusionan de nuevo en la rama `develop`. Es importante nombrar las ramas de características de manera descriptiva y significativa para identificar fácilmente la tarea que se está llevando a cabo.

Todas las funciones nuevas deben residir en su propia rama, que se puede enviar al repositorio central para copia de seguridad/colaboración. Sin embargo, en lugar de ramificarse de main, las ramas feature utilizan la rama develop como rama primaria. Cuando una función está terminada, se vuelve a fusionar en develop. Las funciones no deben interactuar nunca directamente con main.
 
Ten en cuenta que las ramas feature combinadas con la rama develop conforman, a todos efectos, el flujo de trabajo de ramas de función.
Las ramas feature suelen crearse a partir de la última rama develop.
Creación de una rama de función

```sh
git checkout develop
git checkout -b feature_branch
```


## Comandos principales

Algunos de los comandos clave que se utilizan en Gitflow son:

- `git branch`: Muestra todas las ramas del repositorio.
- `git checkout`: Se utiliza para cambiar entre ramas.
- `git merge`: Se utiliza para fusionar una rama en otra.
- `git fetch`: Descarga los cambios del repositorio remoto.
- `git pull`: Descarga los cambios del repositorio remoto y los fusiona con la rama actual.
- `git push`: Envía los cambios locales al repositorio remoto.

## Flujo de trabajo

El flujo de trabajo típico en Gitflow es el siguiente:

1. Crear una rama de características:
```sh
git checkout -b feature/nombre-caracteristica
```

2. Trabajar en la rama de características y hacer cambios.

3. Hacer un commit de los cambios: 
```sh
git commit -m "Mensaje descriptivo del commit"
```

4. Fusionar la rama de características con la rama `develop`: 

```sh
git checkout develop` y `git merge --no-ff feature/nombre-caracteristica
```

5. Crear una rama de liberación: 
```sh
git checkout -b release/version-liberacion
```

6. Realizar pruebas y correcciones de errores en la rama de liberación.

7. Fusionar la rama de liberación con la rama `develop` y la rama `master`: 
```sh
git checkout develop
git merge --no-ff release/version-liberacion
```

```sh
git checkout master
git merge --no-ff release/version-liberacion
```

8. Crear una etiqueta para la versión:
```sh
git tag -a version-liberacion -m "Mensaje descriptivo de la version"
```

9. Eliminar la rama de liberación: 
```sh
git branch -d release/version-liberacion
```

10. Crear una rama de hotfix: 
```sh
git checkout -b hotfix/nombre-hotfix
```

11. Realizar correcciones de errores en la rama de hotfix.

12. Fusionar la rama de hotfix con la rama `develop` y la rama `master`: 
```sh
git checkout develop
git merge --no-ff hotfix/nombre-hotfix
```

```sh
git checkout master
git merge --no-ff hotfix/nombre-hotfix
```